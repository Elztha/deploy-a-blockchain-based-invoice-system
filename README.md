# Deploy a Blockchain-based Invoice System

## Overview

<div align="center">
    <img src="./images/diagram.png" width=40%>
</div>

## Scenario

In this lab, we will deploy an Fabrication-proof Invoice System via Ethereum Blockchain, it will run on a simple Ethereum Framework called [Ganache](https://www.trufflesuite.com/ganache), for the sake of simplicity, our infrastructure would be running on Cloud9 Instance Service Provided by AWS.

## Prerequisites

* Make sure the region is **US East (N. Virginia)**, which its short name is **us-east-1**.

* Download, upload, and unzip [eth-inv.tar.gz](./materials/eth-inv.tar.gz) in Cloud9 later that we will use and create


## Step by step

### Set Up AWS Cloud9 Environment
In this lab, we will be using AWS Cloud9, a powerful cloud IDE integrating programming languages and useful tools. A Cloud9 environment is based on an EC2 instance. We can  develop applications with a browser anywhere.

1. Sign in to the AWS Management Console, and then open [AWS Cloud9 console](https://console.aws.amazon.com/cloud9/).

2. If prompted, type the email address for the AWS account root user, and then choose Next.

3. If a welcome page is displayed, for **New AWS Cloud9 environment**, choose **Create environment**. Otherwise, choose **Create environment**.

4. On the **Name environment**	page, type  `Eth-Invoice-<name>` for your environment. Optionally add a description to your environment.

<div align="center">
    <img src="./images/1.png" width=70%>
</div>

5. Leave everything as default and Select **Next Step**.

6. Select **Create environment**. It might take 30~60 seconds to create your environment.

7. Because we want to accomplish access control by attaching a role ourself, we need to **turn off** the Cloud9 temporarily provided IAM credentials first.

<div align="center">
    <img src="./images/2.png" width=70%>
</div>


8. In [Amazon EC2 console](https://console.aws.amazon.com/ec2/v2/home?#Instances:sort=instanceId), right-Select the EC2 instance named with **`aws-cloud9`** prefix and Select **Instance Settings** -> **Attach/Replace IAM Role**.

9. Select **Create new IAM role**.

10. Select **Create role**.

11. Select **EC2** then Select **Next: Permissions**. Because Cloud9 is based on Amazon EC2, therefore we need to choose EC2.

12. Search and select Polices :
      * AdministratorAccess

13. Next: Review.

14. In **Role Name** field, type **AllowEC2Admin** and Select **Create Role**.

15. Back to Attach/Replace IAM Role panel, Select **Refresh** button, **select the role we just create** and Select **Apply**.

<div align="center">
    <img src="./images/3.png" width=70%>
</div>


16. Go to Security group of Cloud9, edit the inbound by add *all traffic* from *anywhere*

<div align="center">
    <img src="./images/4.png" width=70%>
</div>

17. Back to Cloud9 console, upload [eth-inv.tar.gz](./materials/eth-inv.tar.gz)

18. Unzip with command :

	```
	gunzip eth-inv.tar.gz
	
	tar -xvf eth-inv.tar
	```
>If you receive an error, try changing the file name to **materials_eth-inv.tar.gz**

### Deploying Our Smart Contract Back-end


1. On the Cloud9 Console, we will be running a single Ethereum Node via a simple Nodejs Ethereum Framework, type the command:

	```
	npm i ganache-cli -g
	
	ganache-cli -h 0.0.0.0 -db db
	```

2. We will be deploying our Smart Contract via [Remix](http://remix.ethereum.org/) open it in a new tab, a simple solidity IDE for Ethereum based projects, activate a few plugins that we will use:

<div align="center">
    <img src="./images/plugin.png" width=70%>
</div>

<div align="center">
    <img src="./images/plugin1.png" width=70%>
</div>

<div align="center">
    <img src="./images/plugin2.png" width=70%>
</div>

Then, click on **New File**, and then type in **contract.sol** for the name of the file.
<div align="center">
    <img src="./images/5.png" width=70%>
</div>

3. Copy and paste the file [contract.sol](/materials/contract.sol) to the remix IDE.

<div align="center">
    <img src="./images/6.png" width=70%>
</div>

4. Compile the Solidity Code you just copied by clicking on the compile menu.

<div align="center">
    <img src="./images/7.png" width=70%>
</div>


5. Deploy it into your currently running ethereum node by picking the menu below the compile, then pick on **Web3 Provider** as your environment, type in your Cloud9 EC2-IP for the address, and then click on **OK**, then choose **Deploy**

<div align="center">
    <img src="./images/8.png" width=70%>
</div>

<div align="center">
    <img src="./images/ipcomp.png" width=70%>
</div>

> *You can see your EC2 IP through the EC2 Services page, and choosing your cloud9 instance*

<div align="center">
    <img src="./images/ip.png" width=70%>
</div>

6. Copy the output of your deployment, which is the address of your **Smart Contract**, you will **NEED** it later

<div align="center">
    <img src="./images/9.png" width=70%>
</div>

### Deploying our Node.js Front End Application

1. On the Cloud9 Console, we will be running a our nodejs frontend, but first we need to edit a file named **contract.js** to input our account address (line 3), and smart contract address (line 70).


<div align="center">
    <img src="./images/10.png" width=70%>
</div>

<div align="center">
    <img src="./images/11.png" width=70%>
</div>

2. To get the first account address, pause the ganache-cli currently running by pressing ctrl+c, and then restart it with the same command:

	```
	ganache-cli -h 0.0.0.0 -db db
	```

	Scroll up to see 10 accounts that are preset for your, copy the address of the first account, this is your first account address, copy it to **contract.js**

<div align="center">
    <img src="./images/12.png" width=70%>
</div>

3. The Smart Contract Address, will be the address you retrieved when deploying the smart contract through remix, last sub-chapter on step 6

4. Save the file

### Test Your Application

1. On the Cloud9 Console, open a new terminal and run the Nodejs Front End
	```
	npm start
	```

2. You will see a prompt on which port your application is running on

<div align="center">
    <img src="./images/13.png" width=70%>
</div>

3. Run the your Nodejs Application on a new tab by typing your Cloud9 EC2-IP with the port you are running the application on, request for contract upload on '/upload'

<div align="center">
    <img src="./images/14.png" width=70%>
</div>

4. Input your Client name, email and address, and then the services that you would provide together with the transaction currency used.

<div align="center">
    <img src="./images/15.png" width=70%>
</div>

5. Then Click on **Submit**

### Understanding the Invoice Format

<div align="center">
    <img src="./images/invoice_sample.png" width=70%>
</div>

1. Here is the sample invoice format created through our application

The QR-code embedded inside the invoice is the way of validation for the invoice. When you scan it, it will guide you to a validation route on our application, it will load the whole information of the invoice so that the invoice cannot be fabricated and will always be the same as the submitted input.

When a QR-code is fabricated or the invoice doesn't exist in the blockchain, it won't be able to find the valid the contract whilst, it will show the error.

<div align="center">
    <img src="./images/invoice_error.png" width=70%>
</div>


## Conclusion

After finishing this lab, you have successfully deployed how to build an invoice system with blockchain back-end with fabrication-proof memory, as any invoice made are immutable thus making it immune to fabrication via Blockchain Data Structure being used.

## Clean up

* **Cloud9 Instance**
